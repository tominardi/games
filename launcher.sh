#!/bin/bash

if [ $# -eq 0 ] # s'il n'y a pas de paramètres
then
    echo "Quel jeu voulez vous lancer ?"
    ls datas
    read value
else
    value=$1
fi

if [ -n "$value" ]
then
    if [ -f "datas/$value/launch.sh" ]
    then
        ./datas/$value/launch.sh
    else
        echo "Ce jeu n'existe pas"
    fi
else
    echo "Il faut choisir un jeu..."
fi
