/**
 * Favorites Module
 */
fsUtils = require('../utils/fsUtils.js');
var exec = require('child_process').exec;
require('../utils/stringUtils.js');

var nes = require('./nes.js');
var snes = require('./snes.js');
var md = require('./md.js');
var ms = require('./ms.js');
var n64 = require('./n64.js');
var gb = require('./gb.js');
var mame = require('./mame.js');
var scummvm = require('./scummvm.js');
var residualvm = require('./residualvm.js');
var dos = require('./dosbox.js');
var Promise = require('promise');

module.exports = {

    NAME: "fav",
    CURRENT_USER: null,
    FAVORITES: {},

    /**
     * Returns every Roms find in json,
     */
    discover: function () {
        this.openFile();
        if (this.FAVORITES[this.CURRENT_USER]) {
            return this.FAVORITES[this.CURRENT_USER];
        } else {
            return [];
        }
    },

    /**
     * No changes here
     */
    createObject: function(object) {
        return object;
    },

    /*
     *  switch by types
     */
    launch: function (game) {
        if (game.type == 'nes') {
            nes.launch(game);
        } else if (game.type == 'snes') {
            snes.launch(game);
        } else if (game.type == 'md') {
            md.launch(game);
        } else if (game.type == 'ms') {
            ms.launch(game);
        } else if (game.type == 'n64') {
            n64.launch(game);
        } else if (game.type == 'gb') {
            gb.launch(game);
        } else if (game.type == 'mame') {
            mame.launch(game);
        } else if (game.type == 'scummvm') {
            scummvm.launch(game);
        } else if (game.type == 'residualvm') {
            residualvm.launch(game);
        } else if (game.type == 'dos') {
            dos.launch(game);
        }
    },

    add: function(game) {
        var isAlreadyRegistered = false;
        if (!this.FAVORITES[this.CURRENT_USER]) {
            this.FAVORITES[this.CURRENT_USER] = [];
        }
        this.FAVORITES[this.CURRENT_USER].forEach(function(favorite) {
            if (game.id == favorite.id) {
                isAlreadyRegistered = true;
            }
        });
        if (!isAlreadyRegistered) {
            game.favorite = true;
            this.FAVORITES[this.CURRENT_USER].push(game);
        }
        this.saveFile();
    },
    remove: function(game) {
        var removingIndex = undefined;
        for (var i = 0; i < this.FAVORITES[this.CURRENT_USER].length; i++) {
            if (game.id == this.FAVORITES[this.CURRENT_USER][i].id) {
                removingIndex = i;
            }
        }
        if (removingIndex !== undefined) {
            this.FAVORITES[this.CURRENT_USER].splice(removingIndex, 1);
        }
        this.saveFile();
    },

    saveFile: function() {
        fs.writeFile("db/favorites.json", JSON.stringify(this.FAVORITES), function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("Favorites updated");
        });
    },

    /**
     * Open de favorites file and store it into this.FAVORITES
     */
    openFile: function() {
        var data = fs.readFileSync('db/favorites.json');
        this.FAVORITES = JSON.parse(data);
    }

};
