/**
 * ScummVM Module
 */
fsUtils = require('../utils/fsUtils.js');
var exec = require('child_process').exec;
require('../utils/stringUtils.js');

module.exports = {

    NAME: "scummvm",
    COMMAND: './datas/scummvm/{0}/launch.sh',
    PATH: "./datas/scummvm",
    IMAGES_TYPES: ["png", "jpg", "gif"],
    DEFAULT_IMAGE: "src/img/defaults/scumm.png",
    _imagesFiles: [],

    /**
     * Returns every Roms find in path
     */
    discover: function () {
        var excludedFiles = [];
        var paths = fsUtils.getAllPathFromFolder(this.PATH);
        return paths;
    },

    /**
     * Used for an initial creation
     *
     * * id
     * * name
     * * type
     * * year
     * * category
     * * romPath
     * * capture
     * * background
     */
    createObject: function(pathName) {
        var metadatasStat = fs.statSync(this.PATH+'/'+pathName+'/metadatas.json');
        if (metadatasStat.isFile()) {
            var id = pathName;
            var capture = this.DEFAULT_IMAGE;
            var name = pathName;
            var year = 'n/c';
            var background = '';
            var category = 'n/c';

            try {
                var capturePngStat = fs.statSync(this.PATH+'/'+pathName+'/capture.png');
                if (capturePngStat.isFile()) {
                    capture = this.PATH+'/'+pathName+'/capture.png';
                }
            } catch(e) {
                // do nothing
            }

            try {
                var captureJpgStat = fs.statSync(this.PATH+'/'+pathName+'/capture.jpg');
                if (captureJpgStat.isFile()) {
                    capture = this.PATH+'/'+pathName+'/capture.jpg';
                }
            } catch(e) {
                // do nothing
            }

            try {
                var captureGifStat = fs.statSync(this.PATH+'/'+pathName+'/capture.gif');
                if (captureGifStat.isFile()) {
                    capture = this.PATH+'/'+pathName+'/capture.gif';
                }
            } catch(e) {
                // do nothing
            }

            try {
                var backgroundPngStat = fs.statSync(this.PATH+'/'+pathName+'/background.png');
                if (backgroundPngStat.isFile()) {
                    background = this.PATH+'/'+pathName+'/background.png';
                }
            } catch(e) {
                // do nothing
            }

            try {
                var backgroundJpgStat = fs.statSync(this.PATH+'/'+pathName+'/background.jpg');
                if (backgroundJpgStat.isFile()) {
                    background = this.PATH+'/'+pathName+'/background.jpg';
                }
            } catch(e) {
                // do nothing
            }

            try {
                var backgroundGifStat = fs.statSync(this.PATH+'/'+pathName+'/background.gif');
                if (backgroundGifStat.isFile()) {
                    background = this.PATH+'/'+pathName+'/background.gif';
                }
            } catch(e) {
                // do nothing
            }
            if (background === "" && capture !== this.DEFAULT_IMAGE) {
                background = capture;
            }

            try {
                data = JSON.parse(fs.readFileSync(this.PATH+'/'+pathName+'/metadatas.json', "utf-8"));
                if (data.name) {
                    name = data.name;
                }
                if (data.year) {
                    year = data.year;
                }
                if (data.type) {
                    type = data.type;
                }
                if (data.category) {
                    category = data.category;
                }
            } catch (e) {
                console.log(e);
            }
        }



        var gameObject = {
            id: pathName,
            name: name,
            type: this.NAME,
            year: year,
            category: category,
            romPath: null,
            capture: capture,
            background: background
        };
        return gameObject
    },

    launch: function (game) {
        console.log("command: ", this.COMMAND.format(game.id));
        exec(this.COMMAND.format(game.id), function(error, stdout, stderr) {
            // command output is in stdout
        });
    }

};
