/**
 * NES Module
 */
fsUtils = require('../utils/fsUtils.js');
var exec = require('child_process').exec;
require('../utils/stringUtils.js');

module.exports = {
    NAME: "nes",
    EXTENSIONS: ["nes"],
    COMMAND: "nestopia {0}",
    PATH: "./datas/NES",
    IMAGES_TYPES: ["png", "jpg", "gif"],
    DEFAULT_IMAGE: "src/img/defaults/nes.png",
    _imagesFiles: [],
    _backgroundsFiles: {},

    /**
     * Returns every Roms find in NES path
     */
    discover: function () {
        var nesRoms = [];
        var excludedFiles = [];
        var files = fsUtils.getAllFilesFromFolder(this.PATH);
        files.forEach((function(file) {
            this.EXTENSIONS.forEach(function(extension) {
                if (file.indexOf('.'+extension) !== -1) {
                    nesRoms.push(file);
                }
            });
        }).bind(this));
        return nesRoms;
    },

    /**
     * Used for an initial creation
     *
     * * id
     * * name
     * * type
     * * year
     * * category
     * * romPath
     * * capture
     * * background
     */
    createObject: function(romFileName) {

        // create title from rom file name
        var title = this._createTitle(romFileName);


        var gameObject = {
            id: romFileName,
            name: title,
            type: 'nes',
            year: 'n/c',
            category: 'n/c',
            romPath: this.PATH+'/'+romFileName,
            capture: this._findGameCapture(title),
            background: this._findGameBackground(title)
        };
        return gameObject
    },

    launch: function (game) {
        console.log("command: ", this.COMMAND.format(game.romPath.replace(/[!@#$%^&*()+=\-[\]\\';, {}|":<>?~_]/g, "\\$&")));
        exec(this.COMMAND.format(game.romPath.replace(/[!@#$%^&*()+=\-[\]\\';, {}|":<>?~_]/g, "\\$&")), function(error, stdout, stderr) {
            // command output is in stdout
        });
    },

    /* PRIVATE METHODS */

    /**
     * it search each images files and store it to this._imagesFiles
     * except is it's already done.
     */
    _getImagesFiles: function() {
        if (Object.keys(this._imagesFiles).length === 0) {
            var imagesPath = this.PATH + "/captures";
            files = fsUtils.getAllFilesFromFolder(imagesPath);
            files.forEach((function(aFile) {
                this.IMAGES_TYPES.forEach((function(extension) {
                    if (aFile.indexOf('.'+extension) !== -1) {
                        this._imagesFiles[this._createTitle(aFile)] = this.PATH+"/captures/"+aFile;
                    }
                }).bind(this));
            }).bind(this));
        }
    },

    _getBackgroundsFiles: function() {
        if (Object.keys(this._imagesFiles).length === 0) {
            var imagesPath = this.PATH + "/backgrounds";
            files = fsUtils.getAllFilesFromFolder(imagesPath);
            files.forEach((function(aFile) {
                this.IMAGES_TYPES.forEach((function(extension) {
                    if (aFile.indexOf('.'+extension) !== -1) {
                        this._imagesFiles[this._createTitle(aFile)] = this.PATH+"/backgrounds/"+aFile;
                    }
                }).bind(this));
            }).bind(this));
        }
    },

    _findGameCapture: function(gameId) {
        this._getImagesFiles();
        return this._imagesFiles[gameId] || this.DEFAULT_IMAGE;
    },

    _findGameBackground: function(gameId) {
        this._getBackgroundsFiles();
        return this._backgroundsFiles[gameId] || this._findGameCapture(gameId);
    },

    _createTitle: function(filename) {
        var title = filename;
        this.EXTENSIONS.forEach(function(extension) {
            if (filename.indexOf('.'+extension) !== -1) {
                title = filename.substr(0, filename.indexOf("."+extension));
            }
        });
        this.IMAGES_TYPES.forEach(function(extension) {
            if (filename.indexOf('.'+extension) !== -1) {
                title = filename.substr(0, filename.indexOf("."+extension));
            }
        });
        return title;
    }
};
