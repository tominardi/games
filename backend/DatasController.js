nes = require('./systems/nes.js');
snes = require('./systems/snes.js');
md = require('./systems/md.js');
ms = require('./systems/ms.js');
n64 = require('./systems/n64.js');
gb = require('./systems/gb.js');
mame = require('./systems/mame.js');
scummvm = require('./systems/scummvm.js');
residualvm = require('./systems/residualvm.js');
dos = require('./systems/dosbox.js');
favorites = require('./systems/favorites.js');

module.exports = {
    SYSTEMS: [favorites, dos, nes, snes, md, ms, n64, gb, mame, scummvm, residualvm],
    GAMES_BY_SYSTEMS: {},
    discovered: false,



    syncGamesFromFilesystem: function() {
        this.SYSTEMS.forEach((function(system) {
            var games = [];
            system.discover().forEach((function(filename) {
                game = system.createObject(filename);
                games.push(game);
            }).bind(this));
            this.GAMES_BY_SYSTEMS[system.NAME] = games;
        }).bind(this));
        this.syncFavorites();
    },

    getGamesBySystem: function(system) {
        if (!this.discovered) {
            this.syncGamesFromFilesystem();
            this.discovered = true
        }
        return this.GAMES_BY_SYSTEMS[system];
    },

    refreshFavorites: function() {
        console.log("refreshing favorites...");
        var games = [];
        favorites.discover().forEach((function(filename) {
            game = favorites.createObject(filename);
            games.push(game);
        }).bind(this));
        this.GAMES_BY_SYSTEMS[favorites.NAME] = games;
        this.syncFavorites();
    },

    /**
     * Add the stars on favorites
     */
    syncFavorites: function() {
        // firstly we remove every stars (boring...)
        this.SYSTEMS.forEach((function(system) {
            if (this.GAMES_BY_SYSTEMS[system.NAME] && system.NAME != 'fav') {
                this.GAMES_BY_SYSTEMS[system.NAME].forEach(function(game) {
                    game.favorite = false;
                });
            }
        }).bind(this));
        this.GAMES_BY_SYSTEMS[favorites.NAME].forEach((function(favorite) {
            if (this.GAMES_BY_SYSTEMS[favorite.type]) {
                this.GAMES_BY_SYSTEMS[favorite.type].forEach(function(game) {
                    if (game.id === favorite.id) {
                        game.favorite = true;
                    }
                });
            }
        }).bind(this));
    },

    getGameById: function(system, id) {
        if (!this.discovered) {
            this.syncGamesFromFilesystem();
            this.discovered = true
        }
        var findedGame = null;
        this.GAMES_BY_SYSTEMS[system].forEach(function(game) {
            if (game.id == id) {
                findedGame = game;
            } else {
            }
        });
        return findedGame;
    },

    launchGameById: function(system, id) {
        if (!this.discovered) {
            this.syncGamesFromFilesystem();
            this.discovered = true
        }
        var findedGame = null;
        if (!this.GAMES_BY_SYSTEMS[system]) {
            console.log("System unknow: "+system);
        }
        this.GAMES_BY_SYSTEMS[system].forEach(function(game) {
            if (game.id == id) {
                findedGame = game;
            } else {
            }
        });
        if (findedGame) {
            this.SYSTEMS.forEach(function(iterSystem) {
                if (iterSystem.NAME === system) {
                    iterSystem.launch(findedGame);
                }
            });
        }
    },
}
