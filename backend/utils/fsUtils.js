
fs = filesystem = require('fs'),

module.exports = {
    getAllPathFromFolder: function(dir) {
        var results = [];
        filesystem.readdirSync(dir).forEach(function(file) {
            var fileFullPathed = dir+'/'+file;
            var stat = filesystem.statSync(fileFullPathed);
            if (stat.isDirectory()) {
                results.push(file);
            }
        });
        return results;
    },

    getAllFilesFromFolder: function(dir) {
        var results = [];
        filesystem.readdirSync(dir).forEach(function(file) {
            var fileFullPathed = dir+'/'+file;
            var stat = filesystem.statSync(fileFullPathed);
            if (!stat.isDirectory()) {
                results.push(file);
            }
        });
        return results;
    }
};
