/**
 * GamePortal - a lightweight game selector for Linux
 * Thomas Durey - 2016 - All rights reserved
 *
 * # Guide
 *
 * This is the official documentation
 *
 * ## Available types
 *
 * Currently we support only the following systems :
 *
 * * dos - Dos games with dosbox
 * * scummvm - ScummVM Games
 * * residualvm - ResidualVM Games
 * * ms - Master System with your favority command line emulator
 * * md - Megadrive / Genesis with your favority command line emulator
 * * snes - Super NES with your favority command line emulator
 * * nes - NES with your favority command line emulator
 * * gb - GameBoy with your favority command line emulator
 * * n64 - N64 with your favority command line emulator
 * * mame - Mame games
 */
"use strict";
var http = require('http'),
    url = require('url'),
    fs = require('fs'),
    querystring = require('querystring'),
    favorites = require('./backend/systems/favorites.js'),
    datas = require('./backend/DatasController.js');
var exec = require('child_process').exec;
require('./backend/utils/stringUtils.js');

var _getAllFilesFromFolder = function(dir) {
    var results = [];
    fs.readdirSync(dir).forEach(function(file) {

        var fileFullPathed = dir+'/'+file;
        var stat = fs.statSync(fileFullPathed);
        if (stat.isDirectory()) {
            results.push(file);
        }

    });

    return results;

};

var scripts = [
    "src/js/main.js",
    // managers
    "src/js/managers/popupManager.js",
    "src/js/managers/eventsManager.js",
    "src/js/managers/gamesManager.js",
    "src/js/managers/xhrManager.js",
    // widgets
    "src/js/widgets/chooseUserWidget.js",
    "src/js/widgets/currentSystemWidget.js",
    "src/js/widgets/currentGameWidget.js",
    "src/js/widgets/gamesVisualizerWidget.js"
];

var header = "<html><head><title>Games</title><link rel=\"stylesheet\" type=\"text/css\" href=\"src/css/main.css\" />";

scripts.forEach(function(script) {
    header = header+'<script type="text/javascript" src="'+script+'"></script>';
});


var header = header+"</head><body>";

var body = "<div id=\"loader\"><p>Chargement...</p></div><div id=\"popupLayer\"><div class=\"popup\"></div></div><div id=\"mainLayer\"><div id=\"overlay\"></div><div id=\"container\"><h1 id=\"title\">Games</h1><div id=\"big-picture\"></div></div><div id=\"informations\">";
body += "<div id=\"current-system\"><h2 id=\"system-name\"></h2></div>";
body += "<p><span class=\"label\">Ann&eacute;e:</span> <span id=\"year\"></span></p>";
body += "<p><span class=\"label\">Genre:</span> <span id=\"category\"></span></p>";
body += "<div id=\"game-type\" class=\"\"></div><div id=\"controls\"><div id=\"sound\" class=\"\"></div></div>";
body += "</div><div id=\"border\"></div><div id=\"footer\"><div id=\"games\" class=\"gamesWrapper\"></div></div></div>";

var footer = "<script type=\"text/javascript\" src=\"src/js/start.js\"></script></body></html>";


var gamesController = {

    GAMES_BY_PAGES: 1000,
    DEFAULT_FAVORITE_SYSTEM: 'dos',
    NO_IMAGE: "src/img/noCapture.png",
    currentUser: null,

    page: {
        games: [],

    },

    games: [],
    gamesRetrieved: false,

    /**
     * Retrieve every datas from disk
     */
    retrieveDatas: function() {
        if (!this.gamesRetrieved) {
            var files = _getAllFilesFromFolder("datas");
            files.forEach((function(aDir) {
                try {
                    var metadatasStat = fs.statSync('./datas/'+aDir+'/metadatas.json');
                    if (metadatasStat.isFile()) {
                        var id = aDir;
                        var capture = 'src/img/noCapture.png';
                        var name = aDir;
                        var year = 'n/c';
                        var type = 'unknow';
                        var background = '';
                        var category = 'n/c';

                        try {
                            var capturePngStat = fs.statSync('./datas/'+aDir+'/capture.png');
                            if (capturePngStat.isFile()) {
                                capture = 'datas/'+aDir+'/capture.png';
                            }
                        } catch(e) {
                            // do nothing
                        }

                        try {
                            var captureJpgStat = fs.statSync('./datas/'+aDir+'/capture.jpg');
                            if (captureJpgStat.isFile()) {
                                capture = 'datas/'+aDir+'/capture.jpg';
                            }
                        } catch(e) {
                            // do nothing
                        }

                        try {
                            var captureGifStat = fs.statSync('./datas/'+aDir+'/capture.gif');
                            if (captureGifStat.isFile()) {
                                capture = 'datas/'+aDir+'/capture.gif';
                            }
                        } catch(e) {
                            // do nothing
                        }

                        try {
                            var backgroundPngStat = fs.statSync('./datas/'+aDir+'/background.png');
                            if (backgroundPngStat.isFile()) {
                                background = 'datas/'+aDir+'/background.png';
                            }
                        } catch(e) {
                            // do nothing
                        }

                        try {
                            var backgroundJpgStat = fs.statSync('./datas/'+aDir+'/background.jpg');
                            if (backgroundJpgStat.isFile()) {
                                background = 'datas/'+aDir+'/background.jpg';
                            }
                        } catch(e) {
                            // do nothing
                        }

                        try {
                            var backgroundGifStat = fs.statSync('./datas/'+aDir+'/background.gif');
                            if (backgroundGifStat.isFile()) {
                                background = 'datas/'+aDir+'/background.gif';
                            }
                        } catch(e) {
                            // do nothing
                        }
                        if (background === "" && capture !== 'src/img/noCapture.png') {
                            background = capture;
                        }

                        try {
                            data = JSON.parse(fs.readFileSync('./datas/'+aDir+'/metadatas.json', "utf-8"));
                            if (data.name) {
                                name = data.name;
                            }
                            if (data.year) {
                                year = data.year;
                            }
                            if (data.type) {
                                type = data.type;
                            }
                            if (data.category) {
                                category = data.category;
                            }
                        } catch (e) {
                            console.log(e);
                        }

                        this.games.push('{"id":"'+id+'", "name": "'+name+'", "capture": "'+capture+'", "year": "'+year+'", "type": "'+type+'", "background": "'+background+'", "category": "'+category+'"}');
                    }
                } catch(e) {
                    // do nothing
                }


            }).bind(this));
            this.gamesRetrieved = true;
        }
    },

    /**
     * Return the json part of a page
     */
    getPage: function(pageNumber) {
        if (!pageNumber) {
            pageNumber = 1;
        }
        var start = (pageNumber * this.GAMES_BY_PAGES)-this.GAMES_BY_PAGES;
        var end = start + this.GAMES_BY_PAGES;
        return this.games.slice(start, end);
    },

    /**
     * Main method to get a page
     */
    makeGameFiles: function(pageNumber, system) {
        this.retrieveDatas();
        if (!pageNumber) {
            pageNumber = 1;
        }
        /**
         * Temporary code, for dummy dev - We actually need to filter here, with parameters. No pagination by now
         */
        this.games = datas.getGamesBySystem(system);

        /**
         * End temporary code
         */
         try {
            this.totalPages = Math.ceil(this.games.length / this.GAMES_BY_PAGES);
        } catch(e) {
            this.totalPages = 0;
        }
        var body = '{"page": '+pageNumber+', "totalGames": '+this.games.length+', "totalPages": '+this.totalPages+',"games":[';
        var currentIndex = 0;
        var page = this.getPage(pageNumber);
        page.forEach(function(game) {
            body += JSON.stringify(game);
            if (page.length > currentIndex+1) {
                body += ',';
            }
            currentIndex += 1;

        });
        body += "]}";
        return body;
    },

    /*  check if game is on favorites. If not, we add it */
    addToFavorite: function(game) {
        favorites.add(game);
    },

    /*  check if game is on favorites. If yes, we delete it */
    removeFromFavorite: function(game) {
        favorites.remove(game);
    },
}



var server = http.createServer(function(req, res) {
    if(req.url.indexOf('.css') != -1){ //req.url has the pathname, check if it conatins '.css'
        fs.readFile(__dirname + '/' + req.url, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, {'Content-Type': 'text/css'});
            res.write(data);
            res.end();
        });
    } else if (req.url.indexOf('.png') != -1) {
        var img = fs.readFileSync(__dirname + '/' + decodeURIComponent(req.url));
        res.writeHead(200, {'Content-Type': 'image/png' });
        res.end(img, 'binary');
    } else if (req.url.indexOf('.jpg') != -1) {
        var img = fs.readFileSync(__dirname + '/' + decodeURIComponent(req.url));
        res.writeHead(200, {'Content-Type': 'image/jpg' });
        res.end(img, 'binary');
    } else if (req.url.indexOf('.gif') != -1) {
        var img = fs.readFileSync(__dirname + '/' + decodeURIComponent(req.url));
        res.writeHead(200, {'Content-Type': 'image/gif' });
        res.end(img, 'binary');
    } else if (req.url.indexOf('.mp3') != -1) {
        var img = fs.readFileSync(__dirname + '/' + decodeURIComponent(req.url));
        res.writeHead(200, {'Content-Type': 'audio/mpeg' });
        res.end(img, 'binary');
    } else if (req.url.indexOf('/launch') != -1) {
        res.writeHead(200, {"Content-Type": "text/html"});
        var params = querystring.parse(url.parse(req.url).query);
        if (params["gameId"]) {
            datas.launchGameById(params["system"], params["gameId"]);
            res.end("started: "+params["gameId"]);
        } else {
            res.end("no gameId");
        }
    } else if (req.url.indexOf('/setCurrentUser') != -1) {
        res.writeHead(200, {"Content-Type": "text/html"});
        var params = querystring.parse(url.parse(req.url).query);
        if (params["username"]) {
            gamesController.currentUser = params["username"];
            favorites.CURRENT_USER = params["username"];
            res.end("currentUser: "+params["username"]);
        } else {
            res.end("no username");
        }
    } else if (req.url.indexOf('/resizeScreen') != -1) {
        exec("xrandr --size 800x600",  function(error, stdout, stderr) {});
        // empty 200 OK response for now
        res.writeHead(200, "OK", {'Content-Type': 'text/html'});
        res.end();
    } else if (req.url.indexOf('/addToFavorite') != -1) {
        req.on('data', function(chunk) {
            var game = JSON.parse(chunk.toString());
            gamesController.addToFavorite(game);
        });
        req.on('end', function() {
          // empty 200 OK response for now
          res.writeHead(200, "OK", {'Content-Type': 'text/html'});
          res.end();
        });
    } else if (req.url.indexOf('/removeFromFavorite') != -1) {
        req.on('data', function(chunk) {
            var game = JSON.parse(chunk.toString());
            gamesController.removeFromFavorite(game);
        });
        req.on('end', function() {
          // empty 200 OK response for now
          res.writeHead(200, "OK", {'Content-Type': 'text/html'});
          res.end();
        });
    } else if (req.url.indexOf('games.json') != -1) {

        var params = querystring.parse(url.parse(req.url).query);
        datas.refreshFavorites();

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(gamesController.makeGameFiles(params["page"], params["system"]));
        res.end();
    } else if (req.url.indexOf('.js') != -1) {
        fs.readFile(__dirname + '/' + req.url, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            res.end();
        });
    } else {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(header+body+footer);
    }
});
server.listen(8080);

