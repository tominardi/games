window.gap = {
    appReady: false,
    BLIP: new Audio('src/sounds/blip.mp3'),
    BIP: new Audio('src/sounds/bip.mp3'),
    MUSIC: new Audio('src/sounds/music.mp3'),
    SOUND_ENABLED: false,
    FAVORITE_SYSTEM: "dos",
    EVENTS_ENABLED: true,
    SYSTEMS: ["fav", "dos", "ms", "md", "snes", "nes", "gb", "n64", "mame", "scummvm", "residualvm"],
    SYSTEMS_NAMES: {"dos": "DOS", "scummvm": "ScummVM", "residualvm": "ResidualVM", "ms": "Master System", "md": "Mega Drive", "snes": "Super NES", "nes": "NES", "gb": "Game Boy", "n64": "N 64", "mame": "Arcade", "fav": "Favoris"},
    CURRENT_SYSTEM: "dos",
    LOADED_GAMES: [],
    FOCUSED_GAME: null,
    currentAnimation: 0,
    widgets: {},
    currentUser: null,

    startApp: function() {
        this.getUsers()
            .then(this.chooseCurrentUser)
            .then(this.loadGames)
            .then(this.appStarted);
    },

    appStarted: function() {
        gap.stopLoading();
        gap.MUSIC.loop = true;


        /* ============== Start Gamepad ==============
                               (  )
                                ||
                                ||
                            ___|""|__.._
                           /____________\
                           \____________/~~~.
        ============================================= */
        var haveEvents = 'GamepadEvent' in window;
        var haveWebkitEvents = 'WebKitGamepadEvent' in window;
        var controllers = {};
        var detailedAxes = ["lu", "ld", "ll", "lr", "ru", "rd", "rl", "rr"];
        var rAF = window.requestAnimationFrame ||
                  window.mozRequestAnimationFrame ||
                  window.webkitRequestAnimationFrame;

        var currentlyActive = [];
        var axesActive = [];

        function connecthandler(e) {
          addgamepad(e.gamepad);
        }
        function addgamepad(gamepad) {
            controllers[gamepad.index] = gamepad;

            rAF(updateStatus);
        }

        function disconnecthandler(e) {
          removegamepad(e.gamepad);
        }

        function removegamepad(gamepad) {
          delete controllers[gamepad.index];
        }

        function hitButton(number) {
            if (number === 8) {
                gap.toggleSound();
            }
            if (number === 9) {
                gap.eventsManager.hit();
            }
        }

        function updateStatus() {
            if (document.hasFocus()) {
              scangamepads();
              for (j in controllers) {
                var controller = controllers[j];
                for (var i=0; i<controller.buttons.length; i++) {
                    var val = controller.buttons[i];
                    var pressed = val == 1.0;
                    if (typeof(val) == "object") {
                        pressed = val.pressed;
                        val = val.value;
                    }
                    var pct = Math.round(val * 100) + "%";

                    if (pressed) {
                        if (currentlyActive.indexOf(i) === -1) {
                            currentlyActive.push(i);
                        }
                    } else {
                        if (currentlyActive.indexOf(i) !== -1) {
                            currentlyActive.pop(currentlyActive.indexOf(i));

                            console.log("on a tapé le bouton : "+i);
                            hitButton(i);
                        }
                    }
                }

                for (var i=0; i<controller.axes.length; i++) {
                  if (controller.axes[i] != 0) {
                    if (i == 1) { // gauche, haut/bas
                        if (controller.axes[i] > 0) {
                            if (axesActive.indexOf("ld") === -1) {
                                axesActive.push("ld")
                            }

                        } else {
                            if (axesActive.indexOf("lu") === -1) {
                                axesActive.push("lu")
                            }
                        }
                    }
                    if (i == 0) { // gauche, gauche/droite
                        if (controller.axes[i] < 0) {
                            if (axesActive.indexOf("ll") === -1) {
                                axesActive.push("ll")
                            }
                        } else {
                            if (axesActive.indexOf("lr") === -1) {
                                axesActive.push("lr")
                            }
                        }
                    }
                    if (i == 3) { // droit, haut/bas
                        if (controller.axes[i] > 0) {
                            if (axesActive.indexOf("rd") === -1) {
                                axesActive.push("rd")
                            }

                        } else {
                            if (axesActive.indexOf("ru") === -1) {
                                axesActive.push("ru")
                            }
                        }

                    }
                    if (i == 2) { // droit, gauche/droite
                        if (controller.axes[i] < 0) {
                            if (axesActive.indexOf("rl") === -1) {
                                axesActive.push("rl")
                            }
                        } else {
                            if (axesActive.indexOf("rr") === -1) {
                                axesActive.push("rr")
                            }
                        }
                    }

                  } else { // regardons si on relache
                    if (i == 1) { // gauche, haut/bas
                            if (axesActive.indexOf("ld") !== -1) {
                                axesActive.pop(axesActive.indexOf("ld"));
                                gap.eventsManager.down();
                            }
                            if (axesActive.indexOf("lu") !== -1) {
                                axesActive.pop(axesActive.indexOf("lu"));
                                gap.eventsManager.up();
                            }
                    }
                    if (i == 0) { // gauche, gauche/droite
                            if (axesActive.indexOf("ll") !== -1) {
                                axesActive.pop(axesActive.indexOf("ll"));
                                gap.eventsManager.left();
                            }

                            if (axesActive.indexOf("lr") !== -1) {
                                axesActive.pop(axesActive.indexOf("lr"));
                                gap.eventsManager.right();
                            }
                    }
                    if (i == 3) { // droit, haut/bas


                            if (axesActive.indexOf("rd") !== -1) {
                                axesActive.pop(axesActive.indexOf("rd"));
                                console.log("sticke droit : vers le bas");
                            }


                            if (axesActive.indexOf("ru") !== -1) {
                                axesActive.pop(axesActive.indexOf("ru"));
                                console.log("sticke droit : vers le haut");
                            }

                    }
                    if (i == 2) { // droit, gauche/droite
                            if (axesActive.indexOf("rl") !== -1) {
                                axesActive.pop(axesActive.indexOf("rl"));
                                console.log("sticke droit : vers la gauche");
                            }
                            if (axesActive.indexOf("rr") !== -1) {
                                axesActive.pop(axesActive.indexOf("rr"));
                                console.log("sticke droit : vers la droite");
                            }

                    }

                  }
                }
                rAF(updateStatus);
            }
            }
        }

        function scangamepads() {
          var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
          for (var i = 0; i < gamepads.length; i++) {
            if (gamepads[i]) {
              if (!(gamepads[i].index in controllers)) {
                addgamepad(gamepads[i]);
              } else {
                controllers[gamepads[i].index] = gamepads[i];
              }
            }
          }
        }

        if (haveEvents) {
          window.addEventListener("gamepadconnected", connecthandler);
          window.addEventListener("gamepaddisconnected", disconnecthandler);
        } else if (haveWebkitEvents) {
          window.addEventListener("webkitgamepadconnected", connecthandler);
          window.addEventListener("webkitgamepaddisconnected", disconnecthandler);
        } else {
          setInterval(scangamepads, 500);
        }

        // already plugged ?
        var arrayGP = navigator.getGamepads();
        [0, 1, 2, 3].forEach(function(key) {
            if (arrayGP[key] != undefined) {
                rAF(updateStatus);
            }
        });
        /* ============== Stop Gamepad ===============
                               (  )
                                ||
                                ||
                            ___|""|__.._
                           /____________\
                           \____________/~~~.
        ============================================= */


        document.addEventListener('keydown', function(e) {
            if (gap.EVENTS_ENABLED) {
                if (e.keyIdentifier === "Up") {
                    gap.eventsManager.up();
                } else if (e.keyIdentifier === "Down") {
                    gap.eventsManager.down();
                } else if (e.keyIdentifier === "Left") {
                    gap.eventsManager.left();
                } else if (e.keyIdentifier === "Right") {
                    gap.eventsManager.right();
                } else if (e.keyIdentifier === "Enter") {
                    gap.eventsManager.hit();
                } else if (e.keyIdentifier === "U+004A") { // toggle Sound
                    gap.toggleSound();
                } else if (e.keyIdentifier === "U+0046") { // switch favorites
                    gap.eventsManager.toggleFavorite();
                }
            }
        });
        window.addEventListener('focus', function() {
            setTimeout(function() { // wait because some emulators give back the focus for a short moment (like dosbox)
                if (document.hasFocus()) {
                    gap.EVENTS_ENABLED = true;
                    if (gap.SOUND_ENABLED) {
                        gap.MUSIC.play();
                    }
                    return gap.xhrManager.doXhr("/resizeScreen"); // secure it for some emulators
                }
            }, 500);
        });

        if (gap.SOUND_ENABLED) {
            document.getElementById("sound").className = "enabled";
            gap.MUSIC.play();
        }
        var soundNode = document.getElementById("sound");
        soundNode.addEventListener("click", (function() {
            this.toggleSound();
        }).bind(this));
    },

    loadGames: function() {
        return gap.xhrManager.loadInitialGames()
            .then(function(response) {
                gap.LOADED_GAMES = response.games;
                gap.FOCUSED_GAME = gap.LOADED_GAMES[0];
                return Promise.resolve(true);
            })
            .then(function() {
                return gap.widgets.currentGameWidget.updateCurrentGame(gap.FOCUSED_GAME);
            })
            .then(function() {
                return gap.widgets.currentSystemWidget.refreshCurrentSystem();
            })
            .then(gap.widgets.gamesVisualizerWidget.initialize);
    },

    chooseCurrentUser: function(response) {
        var users = JSON.parse(response.responseText);
        return new Promise((function(resolve) {
            gap.popupManager.displayMessage({
                htmlContent: gap.widgets.chooseUserWidget.makeWidget(this.users),
                validate: function() {

                    resolve(true);
                }
            });
            document.addEventListener('keydown', function(e) {
                    if (e.keyIdentifier === "Left") {
                        gap.widgets.chooseUserWidget.left();
                    } else if (e.keyIdentifier === "Right") {
                        gap.widgets.chooseUserWidget.right();
                    } else if (e.keyIdentifier === "Enter") {
                        gap.widgets.chooseUserWidget.hit();
                    }
                });

        }).bind({users: users}));
    },

    getUsers: function() {
        return gap.xhrManager.doXhr("/db/users.json");
    },

    startLoading: function() {
        document.body.className = "";
    },

    stopLoading: function() {
        document.body.className = "appReady";
    },

    toggleSound: function() {
        if (this.SOUND_ENABLED) {
            var soundNode = document.getElementById("sound");
            soundNode.className = "";
            this.MUSIC.pause();
            this.SOUND_ENABLED = false;
        } else {
            var soundNode = document.getElementById("sound");
            soundNode.className = "enabled";
            this.MUSIC.play();
            this.SOUND_ENABLED = true;
        }
    }

};
