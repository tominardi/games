gap.popupManager = {
    displayMessage: function(options) {
        if (this._isPopupDisplayed()) {
            this._removePopup();
        }
        this._createPopup(options);
    },
    _createPopup: function(options) {
        var popup = popupLayer.getElementsByClassName('popup')[0];
        if (options.title) {
            var title = document.createElement('h2');
            title.textContent = options.title;
            popup.appendChild(title);
        }
        if (options.message) {
            var content = document.createElement('p');
            content.textContent = options.message;
            popup.appendChild(content);
        }
        if (options.htmlContent) {
            popup.appendChild(options.htmlContent);
        }
        if (options.validate) {
            this._validationFunction = options.validate;
        } else {
            this.validate = null;
        }
        popup.classList.add("active");
    },
    _removePopup: function() {
        var popup = popupLayer.getElementsByClassName('popup')[0];
        popup.classList.remove("active");
        setTimeout(function() {
            var popup = popupLayer.getElementsByClassName('popup')[0];
            popup.textContent = "";
        }, 500);

    },
    _isPopupDisplayed: function() {
        var popup = document.getElementsByClassName("popup")[0];
        return popup.textContent != "";
    },

    validate: function() {
        if (this._validationFunction) {
            this._validationFunction();
            this._removePopup();
        }
    }
};
