gap.xhrManager = {
    doXhr: function(url, options) {
        if (!options) {
            options = {};
        }
        return new Promise((function(resolve, reject) {
            var method = "GET";
            if (options.method) {
                method = options.method;
            }
            var xhr = new XMLHttpRequest();
            xhr.open(method, url);
            if (options.contentType) {
                xhr.setRequestHeader("Content-type", options.contentType);
            }
            var body = null;
            if (options.body) {
                body = options.body;
            }
            xhr.send(body);
            xhr.onreadystatechange = function () {
                var DONE = 4; // readyState 4 means the request is done.
                var OK = 200; // status 200 is a successful return.
                if (xhr.readyState === DONE) {
                    if (xhr.status === OK) {
                        resolve(xhr);
                    } else {
                        reject(xhr);
                    }
                }
            };
        }).bind(this));
    },

    /**
     * We need 6 games for current system
     * It automaticaly add the current filter
     */
    loadInitialGames: function() {
        var url = '/games.json?system='+gap.CURRENT_SYSTEM;
        return this.doXhr(url)
            .then(function(xhr) {
                var response = JSON.parse(xhr.responseText);
                return Promise.resolve(response);
            })
    },


    toggleFavorite: function() {
        var game = gap.FOCUSED_GAME;
        if (gap.widgets.currentGameWidget.isFavorite()) {
            var url = "/removeFromFavorite";
        } else {
            var url = "/addToFavorite";
        }
        options = {body: JSON.stringify(game), method: 'POST', contentType: "application/json"};
        return this.doXhr(url, options);
    }

}
