gap.gamesManager = {

    launchGame: function(game) {
        launchXhr = new XMLHttpRequest();
        launchXhr.open('GET', '/launch?gameId='+game.id.replace("%", "%25")+'&system='+game.type);
        launchXhr.send(null);
    },

    loadPreviousSystem: function() {
        return new Promise(function(resolve) {
            gap.currentAnimation++;
            var currentIndex = gap.SYSTEMS.indexOf(gap.CURRENT_SYSTEM);

            var previousIndex = currentIndex - 1;
            if (previousIndex < 0) {
                previousIndex = gap.SYSTEMS.length - 1;
            }

            var previousSystem = gap.SYSTEMS[previousIndex];
            gap.CURRENT_SYSTEM = previousSystem;
            console.log("Current system is now : "+gap.CURRENT_SYSTEM);
            gap.loadGames()
                .then(function() {
                    gap.currentAnimation --;
                    if (gap.currentAnimation === 0) {
                        gap.stopLoading();
                    }
                });
        });
    },

    loadNextSystem: function() {
        return new Promise(function(resolve) {
            gap.currentAnimation++;
            console.log("current system is: ", gap.CURRENT_SYSTEM);
            var currentIndex = gap.SYSTEMS.indexOf(gap.CURRENT_SYSTEM);

            var nextIndex = currentIndex + 1;
            if (nextIndex == gap.SYSTEMS.length) {
                nextIndex = 0;
            }

            var nextSystem = gap.SYSTEMS[nextIndex];
            gap.CURRENT_SYSTEM = nextSystem;
            console.log("Current system is now : "+gap.CURRENT_SYSTEM);
            gap.loadGames()
                .then(function() {
                    gap.currentAnimation --;
                    if (gap.currentAnimation === 0) {
                        gap.stopLoading();
                    }
                });
        });
    },

    selectNextGame: function() {
        return new Promise(function(resolve) {
            var currentIndex = gap.LOADED_GAMES.indexOf(gap.FOCUSED_GAME); // TODO display an error if -1 !
            var nextIndex = currentIndex +1;
            if (nextIndex >= gap.LOADED_GAMES.length) {
                nextIndex = 0;
            }
            gap.FOCUSED_GAME = gap.LOADED_GAMES[nextIndex];
            resolve(gap.FOCUSED_GAME);
        });
    },

    selectPreviousGame: function() {
        return new Promise(function(resolve) {
            var currentIndex = gap.LOADED_GAMES.indexOf(gap.FOCUSED_GAME); // TODO display an error if -1 !
            var nextIndex = currentIndex -1;
            if (nextIndex < 0) {
                nextIndex = gap.LOADED_GAMES.length - 1;
            }
            gap.FOCUSED_GAME = gap.LOADED_GAMES[nextIndex];
            resolve(gap.FOCUSED_GAME);
        });
    }
};
