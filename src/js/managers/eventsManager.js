gap.eventsManager = {


    up: function() {
        gap.startLoading();
        setTimeout(function() {
            gap.gamesManager.loadPreviousSystem()
                .then(gap.stopLoading);
        }, 600);
    },


    down: function() {
        gap.startLoading();
        setTimeout(function() {
            gap.gamesManager.loadNextSystem()
                .then(gap.stopLoading);
        }, 600);
    },


    left: function() {
        if (gap.SOUND_ENABLED) {
            gap.BIP.currentTime = 0;
            gap.BIP.play();
        }
        gap.gamesManager.selectPreviousGame()
            .then(gap.widgets.currentGameWidget.updateCurrentGame.bind(gap.widgets.currentGameWidget))
            .then(gap.widgets.gamesVisualizerWidget.left.bind(gap.widgets.gamesVisualizerWidget));
    },
    right: function() {
        if (gap.SOUND_ENABLED) {
            gap.BIP.currentTime = 0;
            gap.BIP.play();
        }
        gap.gamesManager.selectNextGame()
            .then(gap.widgets.currentGameWidget.updateCurrentGame.bind(gap.widgets.currentGameWidget))
            .then(gap.widgets.gamesVisualizerWidget.right.bind(gap.widgets.gamesVisualizerWidget));
    },


    hit: function() { // OK is pressed
        // works with gap.FOCUSED_GAME
        if (gap.SOUND_ENABLED) {
            gap.BLIP.currentTime = 0;
            gap.BLIP.play();
            gap.MUSIC.pause();
        }
        gap.EVENTS_ENABLED = false;
        gap.gamesManager.launchGame(gap.FOCUSED_GAME);
    },

    toggleFavorite: function() {
        gap.xhrManager.toggleFavorite()
            .then(gap.widgets.currentGameWidget.toggleFavorite.bind(gap.widgets.currentGameWidget));
    }

};
