document.addEventListener('DOMContentLoaded', function() {

    gap.widgets.currentGameWidget = {
        _bigPictureNode: document.getElementById("big-picture"),
        _titleNode: document.getElementById("title"),
        _yearNode: document.getElementById("year"),
        _categoryNode: document.getElementById("category"),
        _gameTypeNode: document.getElementById("game-type"),

        updateCurrentGame: function(currentGame) {
            if (currentGame === undefined) {
                currentGame = {
                    "background": '',
                    "name": 'Aucun jeu',
                    "year": 'n/c',
                    "category": "n/c",
                    "type": "",
                    "favorite": false
                }
            }
            return new Promise((function(resolve) {
                if (currentGame.background) {
                    this._bigPictureNode.style.background = "url("+encodeURIComponent(currentGame.background)+") 0 0 no-repeat";
                    this._bigPictureNode.style.backgroundSize = "100% 100%";
                } else {
                    this._bigPictureNode.style.background = "";
                }
                this._titleNode.textContent = currentGame.name;
                // year
                this._yearNode.textContent = currentGame.year;
                // category
                this._categoryNode.textContent = currentGame.category;
                // type
                this._gameTypeNode.className = currentGame.type;
                resolve(true);
                if (currentGame.favorite) {
                    this._bigPictureNode.className = "favorite";
                } else {
                    this._bigPictureNode.className = "";
                }
            }).bind(this));
        },

        toggleFavorite: function() {
            if (this._bigPictureNode.className === "favorite") {
                this._bigPictureNode.className = "";
            } else {
                this._bigPictureNode.className = "favorite";
            }
        },

        isFavorite: function() {
            return this._bigPictureNode.className === "favorite";
        }
    }

});
