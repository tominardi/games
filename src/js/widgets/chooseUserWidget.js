document.addEventListener('DOMContentLoaded', function() {

    gap.widgets.chooseUserWidget = {
        init: function() {},

        createTile: function(user) {
            var userNode = document.createElement('div');
            userNode.className = "user";
            userNode.setAttribute('data-username', user.username);

            var imageNode = document.createElement('img');
            imageNode.setAttribute('src', "./src/img/avatars/"+user.avatar);
            userNode.appendChild(imageNode);

            var nameNode = document.createElement('span');
            nameNode.className = "name";
            nameNode.textContent = user.name;
            userNode.appendChild(nameNode);
            return userNode;
        },

        makeWidget: function(users) {
            var widgetNode = document.createElement('div');
            widgetNode.className = "chooseUser";
            var titleNode = document.createElement('h2');
            titleNode.textContent = "Choisissez l'utilisateur actuel";
            widgetNode.appendChild(titleNode);
            users.forEach((function(user) {
                widgetNode.appendChild(this.createTile(user));
            }).bind(this));
            widgetNode.getElementsByClassName("user")[0].className = "user focus";
            this.widgetNode = widgetNode;
            return widgetNode;
        },

        left: function() {
            var focusedNode = this.widgetNode.getElementsByClassName("focus")[0];
            var usersNodes = this.widgetNode.getElementsByClassName("user");
            var currentIndex = 0;
            for (var i=0; i < usersNodes.length; i++) {
                if (focusedNode === usersNodes[i]) {
                    currentIndex = i;
                }
            }
            currentIndex --;
            if (currentIndex < 0) {
                currentIndex = usersNodes.length - 1;
            }
            focusedNode.className = "user";
            usersNodes[currentIndex].className = 'user focus';
        },

        right: function() {
            var focusedNode = this.widgetNode.getElementsByClassName("focus")[0];
            var usersNodes = this.widgetNode.getElementsByClassName("user");
            var currentIndex = 0;
            for (var i=0; i < usersNodes.length; i++) {
                if (focusedNode === usersNodes[i]) {
                    currentIndex = i;
                }
            }
            currentIndex ++;
            if (currentIndex >= usersNodes.length) {
                currentIndex = 0;
            }
            focusedNode.className = "user";
            usersNodes[currentIndex].className = 'user focus';
        },

        hit: function() {
            this._setCurrentUser();
            gap.popupManager.validate();

        },

        _setCurrentUser() {
            var focusedNode = this.widgetNode.getElementsByClassName("focus")[0];
            var username = focusedNode.getAttribute('data-username');
            gap.currentUser = username;
            // TODO request the backend to store that value
            changeUserXhr = new XMLHttpRequest();
            changeUserXhr.open('GET', '/setCurrentUser?username='+username);
            changeUserXhr.send(null);
        }

    }

});
