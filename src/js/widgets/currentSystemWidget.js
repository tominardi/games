document.addEventListener('DOMContentLoaded', function() {

        gap.widgets.currentSystemWidget = {
            containerNode: document.getElementById("current-system"),
            titleNode: document.getElementById("system-name"),

            refreshCurrentSystem: function() {
                return new Promise((function(resolve) {
                    this.titleNode.textContent = gap.SYSTEMS_NAMES[gap.CURRENT_SYSTEM];
                    this.containerNode.className = gap.CURRENT_SYSTEM;
                    resolve(true);
                }).bind(this));
            }
        }

});
