document.addEventListener('DOMContentLoaded', function() {

    gap.widgets.gamesVisualizerWidget = {
        _gamesNode: document.getElementById("games"),

        initialize: function() {
            return new Promise((function(resolve) {
                gap.widgets.gamesVisualizerWidget._gamesNode.innerHTML = "";
                // take the 5 first games of gap
                var games = gap.LOADED_GAMES.slice(0, 5);
                games.forEach((function(game) {
                    gap.widgets.gamesVisualizerWidget._gamesNode.appendChild(gap.widgets.gamesVisualizerWidget.makeTileNode(game));
                }).bind(this));
                resolve(true);
            }).bind(this))
        },

        clear: function() {
            gap.widgets.gamesVisualizerWidget._gamesNode.innerHTML = "";
        },

        left: function() {
            this._animateLeft()
                .then(this.refreshGames.bind(this));
        },

        right: function() {
            this._animateRight()
                .then(this.refreshGames.bind(this));
        },

        /**
         * Based on FOCUSED GAMES, will find the 5 next games, including a return to the beginning if we reach the end
         */
        refreshGames: function() {
            var maxNextGames = 5;
            var maxGamesFromBeginning = 0; // used if we are at the end of the list
            var currentIndex = gap.LOADED_GAMES.indexOf(gap.FOCUSED_GAME);
            var games = gap.LOADED_GAMES.slice(currentIndex, currentIndex+maxNextGames);
            if (games.length < maxNextGames) {
                maxGamesFromBeginning = maxNextGames - games.length;
            }
            completeGames = gap.LOADED_GAMES.slice(0, maxGamesFromBeginning);
            games = games.concat(completeGames);
            this.clear();
            games.forEach((function(game) {
                gap.widgets.gamesVisualizerWidget._gamesNode.appendChild(gap.widgets.gamesVisualizerWidget.makeTileNode(game));
            }).bind(this));
        },


        /**
            Return HTML code for a tile
         */
        makeTileNode: function(game) {
            var html = document.createElement('div');
            html.classList.add("game");
            html.setAttribute("data-id", game.id);
            html.setAttribute("data-background", game.background);
            html.setAttribute("data-category", game.category);
            html.setAttribute("data-type", game.type);
            html.setAttribute("data-year", game.year);
            html.style.backgroundImage = "url('"+game.capture+"')";
            var titleNode = document.createElement('h2');
            titleNode.textContent = game.name;
            html.appendChild(titleNode);
            html.addEventListener("click", function() {
                launchGame(this);
            });
            return html;
        },

        // Private methods

        _animateRight: function() {
            return new Promise(function(resolve) {
                var gamesNodes = document.getElementsByClassName('game');
                for (var i=0; i<gamesNodes.length; i++) {
                    gamesNodes[i].style.transform = "translate(-190px)";
                }
                gamesNodes[0].addEventListener("transitionend", function() {
                    resolve(true);
                });
            });
        },

        _animateLeft: function() {
            return new Promise(function(resolve) {
                var gamesNodes = document.getElementsByClassName('game');
                for (var i=0; i<gamesNodes.length; i++) {
                    gamesNodes[i].style.transform = "translate(190px)";
                }
                gamesNodes[0].addEventListener("transitionend", function() {
                    resolve(true);
                });
            });
        }

    }

});
